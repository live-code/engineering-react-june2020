import React from 'react';
import { Redirect, Route } from 'react-router';
import { getItemFromLocalStorage } from './auth.utils';

interface PrivateRouteProps {
  path: string;
  children: React.ReactNode;
}

export const PrivateRoute: React.FC<PrivateRouteProps> = ({ children, ...rest }) => {
  return (
    <Route>
      {
        getItemFromLocalStorage('token') ?
          children :
          <Redirect to={{ pathname: 'login'}} />
      }
    </Route>
  );
};

export const IfLogged: React.FC = ({ children}) => {
  return (
    <Route>
      {
        getItemFromLocalStorage('token') ?
          children :
          null
      }
    </Route>
  );
};
