import React from 'react';
import { DemoControlledFormWithServer } from './pages/forms/DemoControlledFormWithServer';
import { BrowserRouter, Switch, Route, Redirect, useHistory } from 'react-router-dom';
import { DemoReactHookForm } from './pages/forms/DemoReactHookForm';
import { DemoUncontrolledForm } from './pages/forms/DemoUncontrolledForm';
import DemoWeather from './pages/meteo/DemoWeather';
import DemoPanel from './pages/panel/DemoPanel';
import DemoUseEffects from './pages/user/DemoUseEffects';
import { DemoControlledForm } from './pages/forms/DemoControlledForm';
import { NavBar } from './components/NavBar';
import { Catalog } from './pages/Catalog';
import { PageLogin } from './pages/login/Login';
import { PrivateRoute } from './auth/PrivateRoute';

const App = () => {


  return (
      <div>
        <NavBar/>

      <div className="container mt-3">
        <Switch>


          <Route path="/react-hook-com">
            <DemoReactHookForm />
          </Route>

          <Route path="/form-controlled">
            <DemoControlledForm />
          </Route>

          <PrivateRoute path="/form-controlled-with-server">
            <DemoControlledFormWithServer />
          </PrivateRoute>

          <Route path="/form-uncontrolled-form">
            <DemoUncontrolledForm />
          </Route>

          <Route path="/weather">
            <DemoWeather />
          </Route>

          <Route path="/demo-use-effects">
            <DemoUseEffects />
          </Route>

          <Route path="/demo-panel">
            <DemoPanel/>
          </Route>

          <Route
            path="/catalog/:id"
            component={Catalog} />

          <Route path="/login">
            <PageLogin/>
          </Route>

          <Route path="*">
            <Redirect to={{ pathname: '/login'}} />
          </Route>

        </Switch>
      </div>
      </div>
  )
}




export default App;



