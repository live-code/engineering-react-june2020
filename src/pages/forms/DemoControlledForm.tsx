import React, { useState } from 'react';
import cn from 'classnames';

export interface Member {
  name: string;
  email: string;
}

const INITIAL_FORM_DATA = { name: '', email: ''};

export const DemoControlledForm: React.FC = props => {
  const [formData, setFormData] = useState<Member>(INITIAL_FORM_DATA)
  const [list, setList] = useState<Member[]>([]);
  const [dirty, setDirty] = useState<boolean>(false);

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setFormData({
      ...formData,
      [e.currentTarget.name]: e.currentTarget.value,
    });
    setDirty(true);
  }

  const save = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setFormData(INITIAL_FORM_DATA)
    setList([...list, formData])
  };

  const isNameValid = formData.name !== '';
  const isEmailValid = formData.email.length > 2;
  const valid = isNameValid && isEmailValid;

  return <div>
    <form onSubmit={save}>
      <input
        className={
          cn(
            'form-control',
            {'is-valid': isNameValid},
            {'is-invalid': !isNameValid && dirty},
          )
        }
        name="name" type="text" value={formData.name} onChange={onChangeHandler} placeholder="name"/>
      <input
        className={
          cn('form-control', {'is-valid': isEmailValid}, {'is-invalid': !isEmailValid && dirty} )
        }
        name="email" type="text" value={formData.email} onChange={onChangeHandler} placeholder="email"/>

      <button type="submit" disabled={!valid}>SAVE</button>
    </form>
    <hr/>
    {
      list.map((member, index) => {
        return <li key={index}>{member.name}</li>
      })
    }
  </div>
}

