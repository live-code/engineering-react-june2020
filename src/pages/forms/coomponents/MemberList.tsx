import cn from 'classnames';
import React from 'react';
import { Member } from '../DemoControlledFormWithServer';

interface MemberListProps {
  selectedMember: Member;
  items: Member[];
  onDelete: (member: Member) => void;
  onSetActive: (member: Member) => void;
}
export const MemberList: React.FC<MemberListProps> = props => {
  function deleteHandler(member: Member, event: React.MouseEvent) {
    event.stopPropagation();
    props.onDelete(member);
  }

  return <div>
    {
      props.items.map((member, index) => {
        return (
          <li
            key={member.id || index}
            className={cn(
              'list-group-item',
              {'active': member.id === props.selectedMember?.id}
            )}
            onClick={() => props.onSetActive(member)}
          >
            {member.name}

            <div className="pull-right">
              <i className="fa fa-trash" onClick={(e) => deleteHandler(member, e)} />
            </div>
          </li>
        )
      })
    }
  </div>
}
