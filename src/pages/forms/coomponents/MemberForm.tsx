import cn from 'classnames';
import React, { useEffect, useState } from 'react';
import { Member } from '../DemoControlledFormWithServer';

interface MemberFormProps {
  data: Member;
  onSave: (member: Member) => void;
  onClear: () => void;
}

export const MemberForm: React.FC<MemberFormProps> = props => {
  const [dirty, setDirty] = useState<boolean>(false);
  const [localData, setLocalData] = useState<Member>(props.data)

  useEffect(() => {
    setLocalData(props.data)
  }, [props.data])

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setLocalData({
      ...localData,
      [e.currentTarget.name]: e.currentTarget.value,
    });
    setDirty(true);
    // setError(false);
  }
  const saveHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    props.onSave(localData);
  }

  const isNameValid = props.data.name !== '';
  const isEmailValid = props.data.email.length > 2;
  const valid = isNameValid && isEmailValid;

  return (
    <form onSubmit={saveHandler}>
      <pre>{JSON.stringify(localData)}</pre>
      <input
        className={
          cn(
            'form-control',
            {'is-valid': isNameValid},
            {'is-invalid': !isNameValid && dirty},
          )
        }
        name="name" type="text" value={localData.name} onChange={onChangeHandler} placeholder="name"/>
      <input
        className={
          cn('form-control', {'is-valid': isEmailValid}, {'is-invalid': !isEmailValid && dirty} )
        }
        name="email" type="text" value={localData.email} onChange={onChangeHandler} placeholder="email"/>

      <button type="submit" disabled={!valid}>
        { props.data.id ? 'EDIT' : 'ADD'}
      </button>

      { props.data.id && <button type="button"
                                 onClick={props.onClear}>Clear</button>}
    </form>
  )
}
