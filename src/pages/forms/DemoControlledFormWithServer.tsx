import React from 'react';
import { MemberForm } from './coomponents/MemberForm';
import { MemberList } from './coomponents/MemberList';
import { useMember } from './hooks/useMember';

export interface Member {
  id: number | null;
  name: string;
  email: string;
}

export const DemoControlledFormWithServer: React.FC = props => {

  const {
    error, formData, list,
    save, del, setActive, clear,
  } = useMember()

  return <div>

    <AlertMessage value={error} />

    <MemberForm
      data={formData}
      onSave={save}
      onClear={clear}
    />

    <hr/>
    <MemberList
      selectedMember={formData}
      items={list}
      onDelete={del}
      onSetActive={setActive}
    />
  </div>
}


// ===

const AlertMessage: React.FC<{value: boolean}> = props => {
  return props.value ?
    <div className="alert alert-danger">Errore !!</div> :
    null;
}
