import React from 'react';
import { useForm } from 'react-hook-form';

type Inputs = {
  name: string,
  email: string,
};

export const DemoReactHookForm: React.FC = () => {
  const { register, handleSubmit, watch, errors } = useForm<Inputs>({
    mode: "onChange"
  });

  const onSubmit = (data: any) => console.log(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input name="email" ref={register} />
      <input name="name" ref={register({ required: true })} />
        {errors.name && <span>This field is required</span>}

      <input type="submit" />
    </form>
  )
}
