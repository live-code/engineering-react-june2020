import React, { useEffect, useRef, useState } from 'react';
import { Weather } from '../meteo/components/Weather';

const DEFAULT_CITY = 'Milan';

export const DemoUncontrolledForm: React.FC = props => {
  const inputEl = useRef<HTMLInputElement>(null);
  const [city, setCity] = useState<string>(DEFAULT_CITY);

  useEffect(() => {
    if (inputEl.current) {
      inputEl.current.focus();
      inputEl.current.value = DEFAULT_CITY;
    }
  }, [])

  function onKeyPressHandler(event: React.KeyboardEvent<HTMLInputElement>) {
    if (event.key === 'Enter' && event.currentTarget.value.length > 2) {
      setCity(event.currentTarget.value)
      event.currentTarget.value = '';
    }
  }

  return <div>
    <input type="text" ref={inputEl} onKeyPress={onKeyPressHandler} />
    <Weather city={city}/>
  </div>
}
