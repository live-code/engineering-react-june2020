import { useEffect, useState } from 'react';
import Axios from 'axios';
import { Member } from '../DemoControlledFormWithServer';
import { getItemFromLocalStorage } from '../../../auth/auth.utils';

const INITIAL_FORM_DATA = { id: null, name: '', email: ''};

/*function get(url, params) {
  Axios.get('http://localhost:3001/members', {
    ...params,
    headers: {
      ...params.headers,
        Auth: getItemFromLocalStorage('token') || ''
    }
  })
}*/

export function useMember() {
  const [formData, setFormData] = useState<Member>(INITIAL_FORM_DATA)
  const [list, setList] = useState<Member[]>([]);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {

    Axios.get('http://localhost:3001/members', {
      headers: {
        Auth: getItemFromLocalStorage('token') || ''
      }
    })
      .then(res => {
        setList(res.data)
      })
      .catch(() => setError(true))
  }, [])


  const save = (member: Member) => {
    setError(false);
    if (member.id) {
      edit(member);
    } else {
      add(member);
    }
  };

  const add = (member: Member) => {
    Axios.post('http://localhost:3001/members', member)
      .then(res => {
        setList([...list, res.data]);
        setFormData(INITIAL_FORM_DATA);
        // setDirty(false)
      })
      .catch(() => setError(true))
  }


  const edit = (member: Member) => {
    Axios.put('http://localhost:3001/members/' + member.id, member)
      .then(() => {
        setList(
          list.map(item => {
            return item.id === member.id ? member : item;
          })
        )
      })
      .catch(() => setError(true))
  }




  function deleteHandler(member: Member) {
    Axios.delete('http://localhost:3001/members/' + member.id)
      .then(() => {
        setList(
          list.filter(m => m.id !== member.id)
        );

        if (member.id === formData.id) {
          setFormData(INITIAL_FORM_DATA)
        }
      })
  }


  function setActiveHandler(member: Member) {
    setFormData(member)
  }

  function clearHandler() {
    setFormData(INITIAL_FORM_DATA)
  }

  return {
    formData,
    list,
    error,
    save,
    clear: clearHandler,
    del: deleteHandler,
    setActive: setActiveHandler,
  }

}
