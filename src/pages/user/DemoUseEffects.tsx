import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { User } from './model/user';

const DemoUseEffects = () => {
  const [id, setId] = useState<number | null>(null)

  return <div>
    <button onClick={() => setId(1)}>1</button>
    <button onClick={() => setId(2)}>2</button>
    <button onClick={() => setId(3)}>3</button>
    <button onClick={() => setId(null)}>clean</button>

    <hr/>
    { id ? <UserDetails id={id} /> : <NoUser />}
  </div>
}

export default DemoUseEffects;


// ===============

export const NoUser = () => {
  return <div className="alert alert-danger">Non ci sono utenti</div>
}

// ===============

interface UserDetailsProps {
  id: number | null;
}

export const UserDetails: React.FC<UserDetailsProps> = (props) => {
  const [user, setUser] = useState<User | null>(null)

  useEffect(() => {
    if (props.id) {
      loadUser()
    }
    const timerId = setInterval(() => {
      console.log('timer')
    }, 1000)

    return () => {
      clearInterval(timerId)
    }
  }, [props.id]);

  const loadUser = () => {
    Axios.get(`https://jsonplaceholder.typicode.com/users/${props.id}`)
      .then(res => {
        setUser(res.data)
      })
  }

  return (
    <div className="card">
      <div className="card-header">{user?.name}</div>
      <div>{user?.email}</div>
      <div>{user?.username}</div>
    </div>
  )
}





