import React from 'react';
import cn from 'classnames';
import { Panel } from '../../components/Panel';

const DemoPanel = () => {

  function openUrl(url: string) {
    window.open(url)
  }

  function doSomething() {
    console.log('doSomething')
  }

  return <div className="container mt-3">
    <Panel
      title="Hello MS"
      theme="danger"
      headerStyles={{ border: '2px solid blue'}}
      onItemClick={() => openUrl('http://www.microsoft.com')}
    > bla bla </Panel>

    <Panel
      title="Hello Google"
      theme="danger"
      headerStyles={{ border: '2px solid blue'}}
      onItemClick={() => openUrl('http://www.google.com')}
    > bla bla </Panel>

    <Panel title="Hello DoSomething" theme="dark" onItemClick={doSomething}>
      <input type="text"/>
      <input type="text"/>
      <input type="text"/>
      <input type="text"/>
      <input type="text"/>
    </Panel>

  </div>
}

export default DemoPanel;

