import React from 'react';
import { useHistory } from 'react-router';
import { Credentials } from '../../model/credential';
import Axios from 'axios';
import { setItemLocalStorage } from '../../auth/auth.utils';

export const PageLogin: React.FC = () => {
  let history = useHistory();

  let login = () => {
    const credentials: Credentials = {
      username: 'abc',
      password: '123'
    };

    Axios.get<{ token: string }>(`http://localhost:3001/login?user=${credentials.username}&pass=${credentials.password}`)
      .then((res) => {
        setItemLocalStorage('token', res.data.token)
        history.push('form-controlled-with-server');
      })
  };

  return (
    <div>
      <h3>Login</h3>
      <p>You must log in to view the private pages (Settings & Admin)</p>
      <button onClick={login}>Log in</button>
    </div>
  );
};
