import React, { useState } from 'react';
import { Weather } from './components/Weather';

const DemoWeather = () => {
  const [currentCity, setCurrentCity] = useState<string>('Trieste')

  const changeCity = (city: string) => {
    setCurrentCity(city);
  }


  return <div className="container mt-3">

    <button onClick={() => setCurrentCity('Trieste')}>Trieste</button>
    <button onClick={() => changeCity('Cagliari')}>Cagliari</button>
    <button onClick={() => changeCity('Milan')}>Milan</button>

    <hr/>
    <Weather city={currentCity}/>
  </div>
}

export default DemoWeather;


