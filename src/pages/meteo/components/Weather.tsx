import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { Meteo } from '../model/meteo';
import { Panel } from '../../../components/Panel';

interface WeatherProps {
  city: string;
}

export const Weather: React.FC<WeatherProps> = props => {
  const [meteo, setMeteo] = useState<Meteo | null>(null);
  const [pending, setPending] = useState<boolean>(false);

  useEffect(() => {
    setMeteo(null);
    setPending(true);
    Axios.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${props.city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .then(res => {
        setTimeout(() => {
          setMeteo(res.data);
          setPending(false);
        }, 1000)
      })
  }, [props.city]);
  
  const icon = 'http://openweathermap.org/img/wn/' + meteo?.weather[0].icon + '@2x.png';

  return <Panel title={props.city}>
    {pending && <i className="fa fa-spinner fa-spin fa-3x fa-fw" />}

    {
      (meteo && !pending) && (<>
        <div>temp: {meteo?.main.temp}°</div>
        <div>humidity: {meteo?.main.humidity}</div>
        <div>desc: {meteo?.weather[0].description}</div>
        <img src={icon} alt=""/>
      </>)
    }

  </Panel>
}
