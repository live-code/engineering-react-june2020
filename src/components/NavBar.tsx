import { Link, useLocation} from "react-router-dom"
import { useHistory } from "react-router"
import React from 'react';
import { IfLogged } from '../auth/PrivateRoute';

export const NavBar: React.FC = () => {
  const history = useHistory();
  useLocation();


  const signOutHandler = () => {
    history.push("/login");
  };

  return <div className="btn-group">

    <Link to="/react-hook-com" className="btn btn-outline-primary">
        React Hook Form
    </Link>

    <IfLogged>
        <Link to="/form-controlled-with-server" className="btn btn-outline-primary">
          Form WithServer
        </Link>
    </IfLogged>


    <Link to="/form-controlled" className="btn btn-outline-primary">
        Form Controlled
    </Link>

    <Link to="/weather" className="btn btn-outline-primary">
      weather
    </Link>


    <Link to="/demo-use-effects" className="btn btn-outline-primary">
      demo-use-effects
    </Link>
    <Link to="/catalog/123" className="btn btn-outline-primary">
      Catalog
    </Link>
    <li className="nav-item" onClick={signOutHandler}>
      <div className="nav-link" >Logout</div>
    </li>
  </div>
}
