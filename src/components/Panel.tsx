

// -----------
import React from 'react';
import cn from 'classnames';

interface PanelProps {
  title: string;
  headerStyles?: React.CSSProperties;
  theme?: 'dark' | 'light' | 'danger';
  onItemClick?: () => void;
  icon?: string | null;
}

export const Panel: React.FC<PanelProps> = ({
  children, headerStyles, icon, onItemClick, title, theme
}) => {
  return (<div className="card">

    <div
      style={headerStyles}
      className={cn(
        'card-header',
        { 'bg-dark text-white': theme === 'dark' },
        { 'bg-light': theme === 'light' },
        { 'bg-danger': theme === 'danger' },
      )}
    >
      {title}
      <div className="pull-right">
        {icon && <i className={icon}
                    onClick={onItemClick} />}
      </div>
    </div>

    <div className="card-body">{children}</div>
  </div>)
}

